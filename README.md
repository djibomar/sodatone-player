
# sodatone takehome

You need to install the depencencies. `npm i`
To start the App `npm start`

I have created a components folder inside src:
* **Controls** handles the play/pause, skip and seeks of the song
* **SongDetails** is used to display the artist name and the track name
* **SongMedia** is used to display the artwork if any
