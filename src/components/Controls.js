import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { Button, ProgressBar, Row, Col} from 'react-bootstrap'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faPlay, faPause, faStepForward, faStepBackward } from '@fortawesome/free-solid-svg-icons'

class Controls extends Component {

  state = {
    progress : 0
  }

  handleProgessClick(e) {

    //Get the progress bar element
    const progressBarElmnt = document.getElementById("progress-bar")

    if(progressBarElmnt) {
      //Compute the percentage that was clicked
      var percent = e.nativeEvent.offsetX / progressBarElmnt.clientWidth * 100;

      this.setState({
        progess: percent
      })

      this.props.onSeek(percent)
    }

  }

  componentDidUpdate(prevProps) {

    if(prevProps.percentPlayed != this.props.percentPlayed) {
      this.setState({
        progess: this.props.percentPlayed
      })
    }
  }

  //https://stackoverflow.com/questions/6312993/javascript-seconds-to-time-string-with-format-hhmmss
  formatSeconds (seconds) {
      var sec_num = parseInt(seconds, 10); // don't forget the second param
      var hours   = Math.floor(sec_num / 3600);
      var minutes = Math.floor((sec_num - (hours * 3600)) / 60);
      var seconds = sec_num - (hours * 3600) - (minutes * 60);

      const addHour = hours > 0 ? true : false

      if (hours   < 10) {hours   = "0"+hours;}
      if (minutes < 10) {minutes = "0"+minutes;}
      if (seconds < 10) {seconds = "0"+seconds;}

      var formated = ""

      if(addHour)
        formated = hours + ':'

      return formated + minutes + ':' + seconds;
  }

  render() {

    const { progess } = this.state

    const { playing, playedSeconds, durationSeconds, onPlay, onSkip, onPrevious } = this.props

    const playBtnIcon = playing && playing == true ? faPause : faPlay

    return (
      <div id="crontrols">

        <Row className="controlSection">
          <Col xs={1}></Col>
          <Col>
            <div>
              <ProgressBar
                id="progress-bar"
                now={progess}
                onClick={this.handleProgessClick.bind(this)}
              />
            <span>{this.formatSeconds(playedSeconds)}</span>
            <span style={{float: "right"}}>{this.formatSeconds(durationSeconds)}</span>
            </div>
          </Col>
          <Col xs={1}></Col>
        </Row>

        <Row className="controlSection">
          <Col xs={0} md={true}></Col>
          <Col>
            <Row>
              <Col className="d-flex justify-content-around">
                <Button variant="outline-primary" size="xl" onClick={onPrevious}>
                  <FontAwesomeIcon icon={faStepBackward} />
                </Button>
              </Col>
              <Col className="d-flex justify-content-around">
                <Button variant="primary" size="xl" onClick={onPlay}>
                  <FontAwesomeIcon icon={playBtnIcon} />
                </Button>
              </Col>
              <Col className="d-flex justify-content-around">
                <Button variant="outline-primary" size="xl" onClick={onSkip}>
                  <FontAwesomeIcon icon={faStepForward} />
                </Button>
              </Col>
            </Row>
          </Col>
          <Col xs={0} md={true}></Col>
        </Row>

      </div>
    );

  }
}


Controls.propTypes = {
  playing: PropTypes.bool,
  onPlay: PropTypes.func,
  onSkip: PropTypes.func,
  onPrevious: PropTypes.func,
  onSeek: PropTypes.func,
}

Controls.defaultProps = {
  playing: false,
  playedSeconds : 0,
  durationSeconds : 0
};


export default Controls;
