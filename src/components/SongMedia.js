import React, { Component } from 'react';
import { Row, Col, Image } from 'react-bootstrap'

class SongMedia extends Component {

  render() {

    const { artworkUrl } = this.props

    const imgHtml = artworkUrl ? <Image src={artworkUrl} rounded thumbnail={true} /> : <p>No Image</p>
    return (
      <div id="song-media">
        <Row>
          <Col className="d-flex justify-content-around">
            {imgHtml}
          </Col>
        </Row>
      </div>
    );
  }
}

SongMedia.defaultProps = {
  artworkUrl: ""
};

export default SongMedia;
