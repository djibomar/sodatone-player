import React, { Component } from 'react';
import { Row, Col} from 'react-bootstrap'

class SongDetails extends Component {

  render() {

    const { trackName, artistName } = this.props

    return (
      <div id="song-details">
        <div>
          <h3>{trackName}</h3>
        </div>
        <div>
          <h6>{artistName}</h6>
        </div>
      </div>
    );
  }
}

SongDetails.defaultProps = {
  trackName: "Unknown Track",
  artistName: "Unknown Artist",
};

export default SongDetails;
