import React, { Component } from 'react';
import PlayerTemplate from './PlayerTemplate.js';
import './App.css';
import 'bootstrap/dist/css/bootstrap.min.css';

class App extends Component {
  render() {
    return (
      <div className="container-fluid">
        <PlayerTemplate />
      </div>
    );
  }
}

export default App;
