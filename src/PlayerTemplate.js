import React from "react"
import { render } from "react-dom"
import ReactPlayer from 'react-player'
import { Row, Col} from 'react-bootstrap'
import Controls from './components/Controls'
import SongMedia from './components/SongMedia'
import SongDetails from './components/SongDetails'


/*
The goal is to create an audio player, similar to what you'd find at the bottom of the Spotify app.
All our media files are accessible via URLs, as you can see below in `this.tracks`. We're using a
library called react-player (https://www.npmjs.com/package/react-player) for the actual streaming
logic. Our MediaPlayer component encapsulates a ReactPlayer component.

The Player component should implement the following functionality (in order of priority):
1. It should have a play/pause button. Clicking on the button should play/pause the song
   accordingly.
2. It should display the track name, artist name, and artwork for the given track.
3. It should have next/previous buttons for navigating to the next/previous track in `this.tracks`.
4. Style it! The player should always appear at the bottom of the page, and should take up the full
   width of the screen.
5. A seeker for the song. It should graphically show the amount of the song that has been played
   relative to the total length of the song. When you click within the seeker, it should skip
   to a point in the song based on where you click. Look into progressInterval and onProgress in the
   ReactPlayer library (among others).

Notes:
- Assume for now that we will always have a harcoded playlist in `this.tracks`.
- Feel free to add additional libraries if necessary.
- Prioritize a clean implementation.
- Launch localhost:3000 in the browser to view the result.
*/
class Player extends React.Component {
  constructor(props) {
    super(props);
    // This is the 'playlist' of tracks that we're playing/pausing, navigating through, etc.
    this.tracks = [
      {
        id: 1,
        trackName: "The Pretender",
        artistName: "Foo Fighters",
        artworkUrl: "https://images.sk-static.com/images/media/profile_images/artists/29315/huge_avatar",
        mediaUrl: "https://p.scdn.co/mp3-preview/6aba2f4e671ffe07fd60807ca5fef82d48146d4c?cid=1cef747d7bdf4c52ac981490515bda71",
        durationMilliseconds: 30000 // This track is 30 seconds long (30,000 milliseconds).
      },
      {
        id: 2,
        artistName: "Arctic Monkeys",
        trackName: "Do I Wanna Know?",
        artworkUrl: "https://cps-static.rovicorp.com/3/JPG_500/MI0003/626/MI0003626958.jpg?partner=allrovi.com",
        mediaUrl: "https://p.scdn.co/mp3-preview/9ec5fce4b39656754da750499597fcc1d2cc82e5?cid=1cef747d7bdf4c52ac981490515bda71",
        durationMilliseconds: 30000
      }
    ];
  }
  render() {
    return (
      <div>
        <MediaPlayer
          tracks={this.tracks}
        />
      </div>
    );
  }
}

/*
Library documentation: https://www.npmjs.com/package/react-player
*/
class MediaPlayer extends React.Component {

  state = {
    playing: false,
    trackIndex: 0,
    percentPlayed: 0,
    playedSeconds: 0,
    loadedSeconds: 0
  }

  player = React.createRef();

  //Play/Pause
  handlePlay() {
    let value = !this.state.playing

    this.setState({
      playing : value
    })
  }

  //Skip to next song
  handleSkip() {
    const { tracks } = this.props
    let value = this.state.trackIndex == tracks.length - 1 ? 0 : this.state.trackIndex + 1

    this.setState({
      trackIndex : value
    })
  }

  //Go back to previous song
  handlePrevious() {
    const { tracks } = this.props
    let value = this.state.trackIndex == 0 ? tracks.length - 1: this.state.trackIndex - 1

    this.setState({
      trackIndex : value
    })
  }

  handleSeek(percentPlayed) {
    //Convert the percentage to a value btw 0 and 1 and seek there

    if(percentPlayed > 1)
      this.player.current.seekTo(percentPlayed / 100, 'fraction')
  }

  handeProgress(progress) {

    const { playedSeconds, loadedSeconds } = progress

    if (playedSeconds > 0 && loadedSeconds > 0) {
      const percentPlayed = playedSeconds / loadedSeconds * 100

      this.setState({ percentPlayed, playedSeconds, loadedSeconds })
    }
  }

  //End of the song: Play next or stop
  handleEnded() {
    const { tracks } = this.props

    let state
    //Play next song in our playlist or if at the end of the playlist stop
    if(this.state.trackIndex < tracks.length - 1)
      state = {
        trackIndex : this.state.trackIndex + 1
      }
    else
      state = {
        playing : false,
        percentPlayed: 0,
        playedSeconds: 0,
        trackIndex: 0 //Let's go back at the beginning of the playlist
      }

    this.setState(state)
  }

  isValidTrack(track) {

    if(track == null)
      return false;

    //Make sure mediaUrl is a valid url. This is only thing that is required to play a song
    if (track && typeof track == "object" && track.hasOwnProperty("mediaUrl")) {

      //If it's not an invalid url, an exception is thrown
      try {
        const url = new URL(track.mediaUrl)

        return true
      }catch(e) {
        console.error(e);
      }
    }

    return false
  }

  render() {

    const { playing, playedSeconds, loadedSeconds, trackIndex, percentPlayed } = this.state

    const { tracks } = this.props

    if(!tracks)
      return <div>Cannot Play tracks</div>

    //get the current track to play
    let track = tracks && tracks[trackIndex] ? tracks[trackIndex] : null

    //Skip invalid tracks
    if(track == null || !this.isValidTrack(track)) {
      track = {
        mediaUrl : false
      }

      this.handleSkip()
    }

    //Set track duration in seconds
    track.durationSeconds = track.durationMilliseconds ? (track.durationMilliseconds / 1000) : loadedSeconds

    return (
      <div id="react-player-container" className="border border-3 border-primary rounded align-self-center">

        {track.mediaUrl &&
          <ReactPlayer
            ref={this.player}
            playing={playing}
            height={'0px'}
            width={'0px'}
            config={{ file: { forceAudio: true } }}
            controls={false}
            progressInterval={1000}
            url={track.mediaUrl || ""}
            onProgress={this.handeProgress.bind(this)}
            onEnded={this.handleEnded.bind(this)}
          />
        }

      <Row className="align-items-end">
          <Col lg={2} md={3} className="align-self-center">
            <SongMedia
              artworkUrl={track.artworkUrl}
            />
          </Col>
          <Col className="align-self-center">
            <Row>
                <Col xs={1}></Col>
                <Col>
                  <SongDetails
                    trackName={track.trackName}
                    artistName={track.artistName}
                  />
                </Col>
            </Row>
            <Row>
              <Col>
                <Controls
                  playing={playing}
                  playedSeconds={playedSeconds}
                  percentPlayed={percentPlayed}
                  durationSeconds={track.durationSeconds}
                  onPlay={this.handlePlay.bind(this)}
                  onSkip={this.handleSkip.bind(this)}
                  onPrevious={this.handlePrevious.bind(this)}
                  onSeek={this.handleSeek.bind(this)}
                />
              </Col>
            </Row>
          </Col>
        </Row>

      </div>
    )
  }
}

export default Player;
